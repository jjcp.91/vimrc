" enable syntax processing
syntax enable
" load filetype-specific indent files
filetype plugin indent on
" leader is comma
let mapleader=" "
" FIXME: do I need this?
set nocompatible
" hide buffers instead of closing them
set hidden
" no beep or flash wanted
set noerrorbells visualbell t_vb=
if has('autocmd')
  autocmd GUIEnter * set visualbell t_vb=
endif
" set unix line endings
set fileformat=unix
set fileformats=unix,dos
" find the current `~/.vim` or `~/vimfiles` directory
let $VIMHOME = fnamemodify(globpath(&rtp, 'this-is-my-vim-home'), ':h')
" name of the directory where to store files for |:mkview|.
set viewdir=$VIMHOME/view/

set undodir=$VIMHOME/undo//
set backupdir=$VIMHOME/backup//
set directory=$VIMHOME/swp//

" vim hardcodes background color erase even if the terminfo file does
" not contain bce (not to mention that libvte based terminals
" incorrectly contain bce in their terminfo files). This causes
" incorrect background rendering when using a color theme with a
" background color.
let &t_ut=''


" =============
" Spaces & Tabs
" =============

" number of visual spaces per TAB
set tabstop=4
" how many columns to use when you hit tab in insert mode
set softtabstop=4
" how many columns to insert with the shift commands (<<, >>)
set shiftwidth=4
" insert spaces instead of tabs
set expandtab
" simple auto indentation support
set smartindent
" configure backspace behavior (fix for windows and macos)
set backspace=indent,eol,start


" =========
" UI Config
" =========

" set relative line numbers
set relativenumber
" show current line number
set number
" highlight current line
set cursorline
" visual autocomplete for command menu
set wildmenu
" file types to ignore in the autocomplete view
set wildignore=*.o,*~,*.pyc
" make tab behaviour in autocomplete view be like in bash
set wildmode=list:longest
" redraw only when we need to.
set lazyredraw
" enable truecolor mode
set termguicolors
" Enable mouse support for everything
set mouse=a
" show column 80
set colorcolumn=80
" set the color scheme
colorscheme challenger_deep


" ==========
" GUI Config
" ==========

" use text-mode tabs
set guioptions-=e
" remove menu bar
set guioptions-=m
" remove toolbar
set guioptions-=T
" remove right-hand scroll bar
set guioptions-=r
" remove left-hand scroll bar
set guioptions-=L


" =========
" Searching
" =========

" search as characters are entered
set incsearch
" highlight matches
set hlsearch
" turn off search highlight
nnoremap <leader><cr> :nohlsearch<CR>


" ========
" Movement
" ========

" move vertically by visual line
nnoremap j gj
nnoremap k gk
" move cursor to the window below
map <C-j> <C-W>j
" move cursor to the window above
map <C-k> <C-W>k
" move cursor to the window on the left
map <C-h> <C-W>h
" move cursor to the window on the right
map <C-l> <C-W>l


" ==========
" Key Combos
" ==========

" hotkey to save the current file
nmap <leader>w :w!<cr>
" hotkey to open a new tab
map <leader>tn :tabnew<cr>
" switch tab by number
noremap <leader>1 1gt
noremap <leader>2 2gt
noremap <leader>3 3gt
noremap <leader>4 4gt
noremap <leader>5 5gt
noremap <leader>6 6gt
noremap <leader>7 7gt
noremap <leader>8 8gt
noremap <leader>9 9gt
noremap <leader>0 :tablast<cr>
" switch to next tab
nnoremap L gt
" switch to previous tab
nnoremap H gT


" ================
" Editing Mappings
" ================

" Remap 0 to first non-blank character
nnoremap 0 ^
" Remap ^ to first character
nnoremap ^ 0


" ==========================
" Delete trailing whitespace
" ==========================

func! DeleteTrailingWS()
  exe "normal mz"
  %s/\s\+$//ge
  exe "normal `z"
endfunc

" delete trailing whitespace on save (on any file)
autocmd BufWrite * :call DeleteTrailingWS()


" Plugins
" ================================================


" ===============
" NERDTree Config
" ===============

" Toggle NERDTree panel with Ctrl+n
map <leader><tab> :NERDTreeToggle<CR>
" Ignore files
let NERDTreeIgnore = ['\.pyc$']
" Collapses on the same line directories that have only one child directory.
let NERDTreeCascadeSingleChildDir=0


" ================================================
" Source local settings

if filereadable($VIMHOME. "/local-vimrc")
    source $VIMHOME/local-vimrc
endif
